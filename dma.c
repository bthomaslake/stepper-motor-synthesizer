#include <libopencm3/stm32/dma.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/usart.h>

#include "dma.h"

#define BUFFER_SIZE 256
#define BUFFER_IS_NOT_EMPTY pBuffer->readIndex != pBuffer->writeIndex
#define BUFFER_IS_EMPTY pBuffer->readIndex == pBuffer->writeIndex
#define BUFFER_IS_WRAPPING pBuffer->readIndex > pBuffer->writeIndex
#define BUFFER_READ_INDEX_SHOULD_WRAP pBuffer->readIndex == pBuffer->maxIndex

typedef struct
{
  uint8_t data[BUFFER_SIZE];
  uint8_t readIndex;
  uint8_t writeIndex;
  const uint8_t maxIndex;
} CircularBuffer;

static CircularBuffer *pBuffer;

static inline void updateWriteIndex(void) 
{
  pBuffer->writeIndex = BUFFER_SIZE - dma_get_number_of_data(DMA1, DMA_CHANNEL5);
}

static inline void wrapBufferReadIndex(void)
{
  pBuffer->readIndex = 0;
}

static uint8_t postfixIncrementBufferIndex(void)
{
  if (BUFFER_READ_INDEX_SHOULD_WRAP)
  {
    uint8_t index = pBuffer->readIndex;
    wrapBufferReadIndex();
    return index;
  }
  else
    return (pBuffer->readIndex)++;
}

static void bufferInit(void)
{
  static CircularBuffer buffer = 
  {
    .readIndex = 0,
    .maxIndex = BUFFER_SIZE - 1,
  };
  pBuffer = &buffer;
}

/**********************
 * Public Functions
 * ********************/

uint8_t dmaBufferReadData(void)
{
  updateWriteIndex();
  if (BUFFER_IS_NOT_EMPTY)
  {
    return pBuffer->data[postfixIncrementBufferIndex()];
  }
  else
  { 
    return BUFFER_READ_ERROR_CODE;
  }
}

void dmaInit()
{
  bufferInit();

  rcc_periph_clock_enable(RCC_DMA1);

  dma_channel_reset(DMA1, DMA_CHANNEL5);

  // Peripheral Settings
  dma_set_read_from_peripheral(DMA1, DMA_CHANNEL5);
  dma_set_peripheral_address(DMA1, DMA_CHANNEL5, (uint32_t) &USART1_DR);
  dma_disable_peripheral_increment_mode(DMA1, DMA_CHANNEL5);
  dma_set_peripheral_size(DMA1, DMA_CHANNEL5, DMA_CCR_PSIZE_32BIT);

  // Memory Settings
  dma_enable_memory_increment_mode(DMA1, DMA_CHANNEL5);
  dma_enable_circular_mode(DMA1, DMA_CHANNEL5);
  dma_set_number_of_data(DMA1, DMA_CHANNEL5, 256);
  dma_set_memory_address(DMA1, DMA_CHANNEL5, (uint32_t) pBuffer->data);
  dma_set_memory_size(DMA1, DMA_CHANNEL5, DMA_CCR_MSIZE_8BIT);

  // Interrupt Settings
  dma_set_priority(DMA1, DMA_CHANNEL5, DMA_CCR_PL_VERY_HIGH);
  dma_disable_transfer_error_interrupt(DMA1, DMA_CHANNEL5);
  dma_disable_half_transfer_interrupt(DMA1, DMA_CHANNEL5);
  dma_disable_transfer_complete_interrupt(DMA1, DMA_CHANNEL5);

  usart_enable_rx_dma(USART1);
  dma_enable_channel(DMA1, DMA_CHANNEL5);
}
