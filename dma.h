#ifndef DMA_H
#define DMA_H

#define BUFFER_READ_ERROR_CODE (uint8_t) 0x00

uint8_t dmaBufferReadData(void);
void dmaInit(void);

#endif
