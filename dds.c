#include "FreeRTOS.h"
#include "task.h"

#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/spi.h>
#include <math.h>

#include "dds.h"

/************************
 * The AD9833 doesn't have any MISO on its SPI interface.
 * **********************/
#define DDS_INTERFACE SPI1
#define FSYNC GPIO4
#define SCLK GPIO5
#define SDATA GPIO7
#define DDS_INTERFACE_PORT GPIOA
#define DDS_INTERFACE_PINS FSYNC|SCLK|SDATA

#define DDS_MASTER_CLOCK_FREQUENCY (double) 25000000
#define DDS_PHASE_ACCUMULATOR_BITS (double) 28

static const double CLOCK_RESOLUTION = DDS_MASTER_CLOCK_FREQUENCY / pow(2, DDS_PHASE_ACCUMULATOR_BITS);
#define DDS_LOWER_14_BIT_MASK (uint32_t) 0x00003FFF
#define DDS_UPPER_14_BIT_MASK (uint32_t) 0xFFFFC000

/**********************
 * AD9833 CONTROL REGISTER BITS
 * ********************
 * This converts the hex representation of the
 * bits to match the AD9833 documentation's naming
 * convention. It uses DX to represent each bit in the
 * control register. All bits should be set to zero by 
 * default when sending a command.
 * ********************/
#define D15 (uint16_t) 0x8000
#define D14 (uint16_t) 0x4000
#define D13 (uint16_t) 0x2000
#define D12 (uint16_t) 0x1000
#define D11 (uint16_t) 0x0800
#define D10 (uint16_t) 0x0400
#define D9 (uint16_t) 0x0200
#define D8 (uint16_t) 0x0100
#define D7 (uint16_t) 0x0080
#define D6 (uint16_t) 0x0040
#define D5 (uint16_t) 0x0020
#define D4 (uint16_t) 0x0010
#define D3 (uint16_t) 0x0008
#define D2 (uint16_t) 0x0004
#define D1 (uint16_t) 0x0002
#define D0 (uint16_t) 0x0001

/**********************
 * AD9833 NAMED CONTROL REGISTER BITS
 * If a bit hasn't been named, it is reserved
 * by the controller.
 **********************
 * NOTES ON THE CONTROL REGISTER
 *
 * Frequency Manipulation: Divide the master clock by the value set in the frequency register.
 *
 * Change the value of a frequency register:
 *   The Whole Register:
 *     1st write: B28
 *     2nd write: FREQX | [14 LSBs of frequency division of master clock]
 *     3rd write: FREQX | [14 MSBs of frequency division of master clock]
 *   Half Of The Register:
 *     1st write: HLB (high for MSBs, low for LSBs)
 *     2nd write: FREQX | [14 data bits]
 *
 * Switch frequency registers: use the FSELECT bit.
 *
 * Output a square wave: Set OPBITEN and DIV2.
 *
 * Output a square wave at half the frequency: OPBITEN and leave DIV2 low.
 *
 * Output a triangle wave: Set MODE.
 *
 * Nothing needs to be output? Save power by setting SLEEP1.
 *
 * Only a square wave is being output? Save power by setting SLEEP12.
 * ********************/
#define FREQ1 D15
#define FREQ0 D14
#define B28 D13
#define HLB D12
#define FSELECT D11
#define PSELECT D10
#define RESET D8
#define SLEEP1 D7
#define SLEEP12 D6
#define OPBITEN D5 
#define DIV2 D3
#define MODE D1

void ddsInit(void)
{
  rcc_periph_clock_enable(RCC_GPIOA);
  rcc_periph_clock_enable(RCC_SPI1);
  gpio_set_mode(
      GPIOA, 
      GPIO_MODE_OUTPUT_50_MHZ, 
      GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, 
      GPIO5|GPIO7
  );
  gpio_set_mode(
      GPIOA,
      GPIO_MODE_OUTPUT_50_MHZ,
      GPIO_CNF_OUTPUT_PUSHPULL,
      GPIO4
  );
  spi_reset(DDS_INTERFACE);
  spi_init_master(
      SPI1,
      SPI_CR1_BAUDRATE_FPCLK_DIV_256,
      SPI_CR1_CPOL_CLK_TO_1_WHEN_IDLE,
      SPI_CR1_CPHA_CLK_TRANSITION_1,
      SPI_CR1_DFF_16BIT,
      SPI_CR1_MSBFIRST
  );
  spi_enable_ss_output(SPI1);
  gpio_set(GPIOA, GPIO4);
}

static void ddsSend(uint16_t *data, uint8_t size)
{
  spi_enable(DDS_INTERFACE);
  gpio_clear(GPIOA, GPIO4);
  for (uint8_t i = 0; i < size; ++i)
  {
    spi_xfer(DDS_INTERFACE, data[i]);
  }
  spi_disable(DDS_INTERFACE);
  gpio_set(GPIOA, GPIO4);
}

void ddsSetFrequency(double frequency)
{
  double frequencyBitsFloat = frequency / CLOCK_RESOLUTION;
  uint32_t frequencyBits = round(frequencyBitsFloat);
  uint16_t lowerFrequencyBits = (uint16_t) (frequencyBits & DDS_LOWER_14_BIT_MASK);
  uint16_t upperFrequencyBits = (uint16_t) ((frequencyBits >> 14) & DDS_LOWER_14_BIT_MASK);
  uint16_t commands[] = {
    [0] = B28|OPBITEN|SLEEP12|DIV2, 
    [1] = FREQ0|lowerFrequencyBits, 
    [2] = FREQ0|upperFrequencyBits
  };
  ddsSend(commands, 3);
}

static uint16_t SILENCE_COMMAND[] = {[0] = SLEEP1};

void ddsSilence(void)
{
  ddsSend(SILENCE_COMMAND, 1);
}
