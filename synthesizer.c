#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"

#include "synthesizer.h"
#include "midiProtocol.h"
#include "dds.h"
#include <stdbool.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>


#define POLYPHONY_COUNT (uint8_t) 1
#define MESSAGE_LENGTH (uint8_t) 3
#define MESSAGE_COMMAND_INDEX (uint8_t) 0
#define MESSAGE_NOTE_INDEX (uint8_t) 1
#define MESSAGE_VELOCITY_INDEX (uint8_t) 2


typedef struct 
{
  uint8_t pitch;
  uint8_t velocity;
  bool on;
} Note;

static Note notes[POLYPHONY_COUNT] = {[0 ... (POLYPHONY_COUNT - 1)].on = false};

static void handleMessage(uint8_t *message)
{
  switch (message[MESSAGE_COMMAND_INDEX])
  {
    case NOTE_ON:
      ddsSetFrequency(noteFrequencies[message[MESSAGE_NOTE_INDEX]]);
      break;
    case NOTE_OFF:
      ddsSilence();
      break;
    default:
      ddsSilence();
      break;
  }
}

static bool checkForValidMessage(uint8_t *message, uint8_t index)
{
  bool messageIsValid = false;
  switch (index)
  {
    case MESSAGE_COMMAND_INDEX:
      messageIsValid = (message[index] == NOTE_OFF || message[index] == NOTE_ON);
      break;
    case MESSAGE_NOTE_INDEX:
      messageIsValid = (message[index] <= G9);
      break;
    case MESSAGE_VELOCITY_INDEX:
      messageIsValid = true;
      break;
    default:
      break;
  }
  return messageIsValid;
}

static inline bool checkForCommandMessage(uint8_t word)
{
  return word == NOTE_ON || word == NOTE_OFF;
}

static void synthTask(void *midiQueue)
{
  uint8_t message[3];
  uint8_t index = MESSAGE_COMMAND_INDEX;
  for (;;)
  {
    xQueueReceive(*((QueueHandle_t*) midiQueue), message, portMAX_DELAY);
    while (!checkForCommandMessage(message[0]))
        xQueueReceive(*((QueueHandle_t*) midiQueue), message, portMAX_DELAY);

    while (uxQueueMessagesWaiting(*((QueueHandle_t*) midiQueue)) < 2)
      taskYIELD();

    xQueueReceive(*((QueueHandle_t*) midiQueue), message + MESSAGE_NOTE_INDEX, portMAX_DELAY);
    xQueueReceive(*((QueueHandle_t*) midiQueue), message + MESSAGE_VELOCITY_INDEX, portMAX_DELAY);
    handleMessage(message);
  }
}

void synthInit(QueueHandle_t *midiQueue)
{
  ddsInit();
  ddsSilence();
  xTaskCreate(synthTask, "Synthesizer Task", 256, (void *) midiQueue, configMAX_PRIORITIES - 1, NULL);
}
