#ifndef DDS_H
#define DDS_H

void ddsInit(void);
void ddsSetFrequency(double frequency);
void ddsSilence(void);

#endif
