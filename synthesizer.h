#include "FreeRTOS.h"
#include "queue.h"

#ifndef SYNTHESIZER_H
#define SYNTHESIZER_H

void synthInit(QueueHandle_t *midiQueue);

#endif
