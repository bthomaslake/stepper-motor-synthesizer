#ifndef MIDI_PROTOCOL_H
#define MIDI_PROTOCOL_H

/******************************
 * Standard MIDI Note Numbers
 *
 * Information Source: MIDI 1.0 Detailed Specification
 *
 * The naming convention is inspired by Lilypond notation and the relevant
 * spoken languages (Danish, German, etc.). This was necessary to allow
 * usage of macros where the common notation for sharps (#) and flats (b) would
 * cause issues.
 * ***************************/
#define CN1 (uint8_t) 0
#define CISN1_DESN1 (uint8_t) 1
#define DN1 (uint8_t) 2
#define DISN1_ESN1 (uint8_t) 3
#define EN1 (uint8_t) 4
#define FN1 (uint8_t) 5
#define FISN1_GESN1 (uint8_t) 6
#define GN1 (uint8_t) 7
#define GISN1_ASN1 (uint8_t) 8
#define AN1 (uint8_t) 9
#define AISN1_BESN1 (uint8_t) 10
#define BN1 (uint8_t) 11
#define C0 (uint8_t) 12
#define CIS0_DES0 (uint8_t) 13
#define D0 (uint8_t) 14
#define DIS0_ES0 (uint8_t) 15
#define E0 (uint8_t) 16
#define F0 (uint8_t) 17
#define FIS0_GES0 (uint8_t) 18
#define G0 (uint8_t) 19
#define GIS0_AS0 (uint8_t) 20

// Beginning of piano range

#define A0 (uint8_t) 21
#define AIS0_BES0 (uint8_t) 22
#define B0 (uint8_t) 23
#define C1 (uint8_t) 24
#define CIS1_DES1 (uint8_t) 25
#define D1 (uint8_t) 26
#define DIS1_ES1 (uint8_t) 27
#define E1 (uint8_t) 28
#define F1 (uint8_t) 29
#define FIS1_GES1 (uint8_t) 30
#define G1 (uint8_t) 31
#define GIS1_AS1 (uint8_t) 32
#define A1 (uint8_t) 33
#define AIS1_BES1 (uint8_t) 34
#define B1 (uint8_t) 35
#define C2 (uint8_t) 36
#define CIS2_DES2 (uint8_t) 37
#define D2 (uint8_t) 38
#define DIS2_ES2 (uint8_t) 39
#define E2 (uint8_t) 40
#define F2 (uint8_t) 41
#define FIS2_GES2 (uint8_t) 42
#define G2 (uint8_t) 43
#define GIS2_AS2 (uint8_t) 44
#define A2 (uint8_t) 45
#define AIS2_BES2 (uint8_t) 46
#define B2 (uint8_t) 47
#define C3 (uint8_t) 48
#define CIS3_DES3 (uint8_t) 49
#define D3 (uint8_t) 50
#define DIS3_ES3 (uint8_t) 51
#define E3 (uint8_t) 52
#define F3 (uint8_t) 53
#define FIS3_GES3 (uint8_t) 54
#define G3 (uint8_t) 55
#define GIS3_AS3 (uint8_t) 56
#define A3 (uint8_t) 57
#define AIS3_BES3 (uint8_t) 58
#define B3 (uint8_t) 59
#define C4 (uint8_t) 60
#define CIS4_DES4 (uint8_t) 61
#define D4 (uint8_t) 62
#define DIS4_ES4 (uint8_t) 63
#define E4 (uint8_t) 64
#define F4 (uint8_t) 65
#define FIS4_GES4 (uint8_t) 66
#define G4 (uint8_t) 67
#define GIS4_AS4 (uint8_t) 68
#define A4 (uint8_t) 69
#define AIS4_BES4 (uint8_t) 70
#define B4 (uint8_t) 71
#define C5 (uint8_t) 72
#define CIS5_DES5 (uint8_t) 73
#define D5 (uint8_t) 74
#define DIS5_ES5 (uint8_t) 75
#define E5 (uint8_t) 76
#define F5 (uint8_t) 77
#define FIS5_GES5 (uint8_t) 78
#define G5 (uint8_t) 79
#define GIS5_AS5 (uint8_t) 80
#define A5 (uint8_t) 81
#define AIS5_BES5 (uint8_t) 82
#define B5 (uint8_t) 83
#define C6 (uint8_t) 84
#define CIS6_DES6 (uint8_t) 85
#define D6 (uint8_t) (uint8_t) (uint8_t) (uint8_t) (uint8_t) (uint8_t) (uint8_t) (uint8_t) (uint8_t) 86
#define DIS6_ES6 (uint8_t) 87
#define E6 (uint8_t) (uint8_t) (uint8_t) (uint8_t) (uint8_t) (uint8_t) (uint8_t) (uint8_t) (uint8_t) 88
#define F6 (uint8_t) 89
#define FIS6_GES6 (uint8_t) 90
#define G6 (uint8_t) 91
#define GIS6_AS6 (uint8_t) 92
#define A6 (uint8_t) 93
#define AIS6_BES6 (uint8_t) 94
#define B6 (uint8_t) 95
#define C7 96
#define CIS7_DES7 97
#define D7 98
#define DIS7_ES7 99
#define E7 100
#define F7 101
#define FIS7_GES7 102
#define G7 103
#define GIS7_AS7 104
#define A7 105
#define AIS7_BES7 106
#define B7 107
#define C8 108

// End of piano range

#define CIS8_DES8 109
#define D8 110
#define DIS8_ES8 111
#define E8 112
#define F8 113
#define FIS8_GES8 114
#define G8 115
#define GIS8_AS8 116
#define A8 117
#define AIS8_BES8 118
#define B8 119
#define C9 120
#define CIS9_DES9 121
#define D9 122
#define DIS9_ES9 123
#define E9 124
#define F9 125
#define FIS9_GES9 126
#define G9 127

// System Real Time Messages

#define TIMING_CLOCK 0xF8
#define START 0xFA
#define CONTINUE 0xFB
#define STOP 0xFC

#define SYSTEM_REALTIME_ACTIVE_SENSING 0xFE

// Commands

#define NOTE_OFF (uint8_t) 0x80
#define NOTE_ON (uint8_t) 0x90
#define POLY_KEY_PRESSURE 0xA0
#define CONTROL_CHANGE 0xB0
#define PROGRAM_CHANGE 0xC0
#define CHANNEL_PRESSURE 0xD0
#define PITCH_BEND 0xE0

/*****************************
 * Translation Of Note Names To Frequencies
 * Information Source: https://en.wikipedia.org/wiki/Piano_key_frequencies
 * ***************************/
const double noteFrequencies[128] =
{
  [A0] = 27.50000,
  [AIS0_BES0] = 29.13524,
  [B0] = 30.86771,
  [C1] = 32.70320,
  [CIS1_DES1] = 34.64783,
  [D1] = 36.70810,
  [DIS1_ES1] = 38.89087,
  [E1] = 41.20344,
  [F1] = 43.65353,
  [FIS1_GES1] = 46.24930,
  [G1] = 48.99943,
  [GIS1_AS1] = 51.91309,
  [A1] = 55.00000,
  [AIS1_BES1] = 58.27047,
  [B1] = 61.73541,
  [C2] = 65.40639,
  [CIS2_DES2] = 69.29566,
  [D2] = 73.41619,
  [DIS2_ES2] = 77.78175,
  [E2] = 82.40689,
  [F2] = 87.30706,
  [FIS2_GES2] = 92.49861,
  [G2] = 97.99886,
  [GIS2_AS2] = 103.8262,
  [A2] = 110.0000,
  [AIS2_BES2] = 116.5409,
  [B2] = 123.4708,
  [C3] = 130.8128,
  [CIS3_DES3] = 138.5913,
  [D3] = 146.8324,
  [DIS3_ES3] = 155.5635,
  [E3] = 164.8138,
  [F3] = 174.6141,
  [FIS3_GES3] = 184.9972,
  [G3] = 195.9977,
  [GIS3_AS3] = 207.6523,
  [A3] = 220.0000,
  [AIS3_BES3] = 233.0819,
  [B3] = 246.9417,
  [C4] = 261.6256,
  [CIS4_DES4] = 277.1826,
  [D4] = 293.6648,
  [DIS4_ES4] = 311.1270,
  [E4] = 329.6276,
  [F4] = 349.2282,
  [FIS4_GES4] = 369.9944,
  [G4] = 391.9954,
  [GIS4_AS4] = 415.3047,
  [A4] = 440.0000,
  [AIS4_BES4] = 466.1638,
  [B4] = 493.8833,
  [C5] = 523.2511,
  [CIS5_DES5] = 554.3653,
  [D5] = 587.3295,
  [DIS5_ES5] = 622.2540,
  [E5] = 659.2551,
  [F5] = 698.4565,
  [FIS5_GES5] = 739.9888,
  [G5] = 783.9909,
  [GIS5_AS5] = 830.6094,
  [A5] = 880.0000,
  [AIS5_BES5] = 932.3275,
  [B5] = 987.7666,
  [C6] = 1046.502,
  [CIS6_DES6] = 1108.731,
  [D6] = 1174.659,
  [DIS6_ES6] = 1244.508,
  [E6] = 1318.510,
  [F6] = 1396.913,
  [FIS6_GES6] = 1479.978,
  [G6] = 1567.982,
  [GIS6_AS6] = 1661.219,
  [A6] = 1760.000,
  [AIS6_BES6] = 1864.655,
  [B6] = 1975.533,
  [C7] = 2093.005,
  [CIS7_DES7] = 2217.461,
  [D7] = 2349.318,
  [DIS7_ES7] = 2489.016,
  [E7] = 2637.020,
  [F7] = 2793.826,
  [FIS7_GES7] = 2959.955,
  [G7] = 3135.963,
  [GIS7_AS7] = 3322.438,
  [A7] = 3520.000,
  [AIS7_BES7] = 3729.310,
  [B7] = 3951.066,
  [C8] = 4186.009,
};

#endif
