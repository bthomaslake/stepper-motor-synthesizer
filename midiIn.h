#include "FreeRTOS.h"
#include "queue.h"

#ifndef MIDI_H
#define MIDI_H

void midiInit(QueueHandle_t *midiQueue);

#endif
