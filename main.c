#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include "midiIn.h"
#include "synthesizer.h"

#define MIDI_QUEUE_SIZE (UBaseType_t) 256
#define MIDI_WORD_SIZE (UBaseType_t) sizeof(uint8_t)

QueueHandle_t midiQueueHandle;

static inline void clockInit(void)
{
  rcc_clock_setup_pll(&rcc_hse_configs[RCC_CLOCK_HSE8_72MHZ]);
}

int main(void) 
{
  clockInit();
  midiQueueHandle = xQueueCreate(MIDI_QUEUE_SIZE, MIDI_WORD_SIZE);
  midiInit(&midiQueueHandle);
  synthInit(&midiQueueHandle);
	vTaskStartScheduler();
	for (;;);
}
