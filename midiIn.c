#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/cm3/cortex.h>

#include "FreeRTOS.h"
#include "task.h"
#include "dma.h"
#include "queue.h"
#include "midiIn.h"

#define MIDI_IN USART1
#define MIDI_BAUD 31250
#define MIDI_DATABITS 8
#define MIDI_STOPBITS USART_STOPBITS_1
#define MIDI_PARITY USART_PARITY_NONE
#define MIDI_FLOW_CONTROL USART_FLOWCONTROL_NONE
#define MIDI_MODE_IN USART_MODE_RX

TaskHandle_t midiTaskHandle;

static inline void midiClockInit(void)
{
  rcc_periph_clock_enable(RCC_GPIOA);
  rcc_periph_clock_enable(RCC_USART1);
}

static inline void midiGpioInit(void)
{
  gpio_set_mode(GPIOA, GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT, GPIO_USART1_RX);
  usart_set_mode(MIDI_IN, MIDI_MODE_IN);
  usart_set_baudrate(MIDI_IN, MIDI_BAUD);
  usart_set_databits(MIDI_IN, MIDI_DATABITS);
  usart_set_stopbits(MIDI_IN, MIDI_STOPBITS);
  usart_set_flow_control(MIDI_IN, MIDI_FLOW_CONTROL);
  usart_set_parity(MIDI_IN, MIDI_PARITY);
  usart_enable(MIDI_IN);
}

static inline void debugUartInit(void)
{
  rcc_periph_clock_enable(RCC_USART2);
  gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_USART2_TX);
  usart_set_mode(USART2, USART_MODE_TX);
  usart_set_baudrate(USART2, 38400);
  usart_set_databits(USART2, 8);
  usart_set_stopbits(USART2, USART_STOPBITS_1);
  usart_set_flow_control(USART2, USART_FLOWCONTROL_NONE);
  usart_set_parity(USART2, USART_PARITY_NONE);
  usart_enable(USART2);
}

static inline void midiInterruptInit(void)
{
  usart_enable_idle_interrupt(MIDI_IN);
  nvic_set_priority(NVIC_USART1_IRQ, 0);
  nvic_enable_irq(NVIC_USART1_IRQ);
}

static void midiInTask(void *midiQueue)
{
  uint8_t input;
  for (;;) {
    ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
    while ((input = dmaBufferReadData()) != BUFFER_READ_ERROR_CODE) 
    {
      xQueueSend(*((QueueHandle_t*) midiQueue), &input, portMAX_DELAY);  
    }
  }
}

void midiInit(QueueHandle_t *midiQueue) 
{
  midiClockInit();
  midiGpioInit();
  debugUartInit();
  dmaInit();
  midiInterruptInit();
  xTaskCreate(midiInTask, "MIDI IN Task", 256, (void *) midiQueue, configMAX_PRIORITIES - 1, &midiTaskHandle);
}

void usart1_isr(void)
{
  // IDLE interrupt is cleared by reading USART_SR register, then reading USART_DR
  if (usart_get_flag(MIDI_IN, USART_SR_IDLE))
  { 
    BaseType_t woken = pdFALSE;
    vTaskNotifyGiveFromISR(midiTaskHandle, &woken);
    usart_recv(USART1); 
    portYIELD_FROM_ISR(woken); 
  }
}
